package com.example.pache.examen.connection;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;


import android.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.pache.examen.MapsActivity;
import com.example.pache.examen.interfaces.WebServiceResponse;

/**
 * Created by pache on 26/03/2016.
 */
public class Connection {
    Fragment fragment;
    FragmentActivity fragmentActivity;
    WebServiceResponse conectionInterface;
    public Connection(Fragment fragment){
        this.fragment = fragment;
    }
    public Connection(FragmentActivity fragmentActivity){
        this.fragmentActivity = fragmentActivity;
    }
    public  void requestCatalogue(){
        RequestQueue queue = Volley.newRequestQueue(fragmentActivity);
        conectionInterface = ((MapsActivity)fragmentActivity);
        String url ="http://plataforma.promexico.gob.mx/sys/gateway.aspx?UID=44201dbb-34c4-487a-92ea-2ce6f36e4644&formato=JSON";
        StringRequest stringRequest = new StringRequest(com.android.volley.Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        conectionInterface.responseCataloge(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                conectionInterface.onResponseError(error.getMessage());
            }
        });
        queue.add(stringRequest);
    }

}
