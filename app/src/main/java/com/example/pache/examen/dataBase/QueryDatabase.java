package com.example.pache.examen.dataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;
import android.util.Pair;


import com.example.pache.examen.classes.Oficina;
import com.example.pache.examen.classes.Pregunta;
import com.example.pache.examen.classes.Registro;
import com.example.pache.examen.common.Definitions;
import com.example.pache.examen.common.Utilities;

import java.util.ArrayList;



/**
 * Created by pache on 26/03/2016.
 */
public class QueryDatabase {
    DataBase usdbh;
    SQLiteDatabase db;
    Context context;
    public QueryDatabase(Context context){
        usdbh = new DataBase(context,Definitions.nameDataBase,null,1);
        db = usdbh.getWritableDatabase();
        this.context = context;
    }

    public Boolean exitsUser(Registro user){
        String qry = "select *from "+Definitions.TableUser+" where usuario='" + user.getUsuario() + "' and contraseña='" + user.getContraseña() + "'";
        Cursor c = executeQry(qry);
        if (c!=null && c.moveToFirst()) {
            return true;
        }else{
            Utilities.mensaje("Alerta","Verifica los datos",context);
            return false;
        }
    }


    public Cursor executeQry(String qry){
        if(db != null) {
            Cursor c=db.rawQuery(qry, null);
            return c;
        }
        return null;
    }
    public boolean insertUser(Registro user){
        ContentValues valores = new ContentValues();
        valores.put("usuario",user.getUsuario());
        valores.put("contraseña",user.getContraseña());
        valores.put("nombre", user.getNombre());
        long result = db.insert(Definitions.TableUser, null, valores);
        if(result == -1) {
            Utilities.mensaje("Alerta", "ingresa otro usuario", context);
            return false;
        }else
            return true;
    }
    public boolean inserOficina(ArrayList<Oficina> oficinas){
        for(Oficina oficina : oficinas){
            ContentValues valores = new ContentValues();
            valores.put("office",oficina.getOffice() == null  ? "" : oficina.getOffice());
            valores.put("street_number",oficina.getStreet_number());
            valores.put("location", oficina.getLocation());
            valores.put("zip_code", oficina.getZip_code());
            valores.put("state",oficina.getState());
            valores.put("country", oficina.getCountry());
            valores.put("agent",oficina.getAgent());
            valores.put("position",oficina.getPosition());
            valores.put("email",oficina.getEmail() == null  ? "" : oficina.getEmail());
            valores.put("phone",oficina.getPhone());
            Pair pair = oficina.getCoordinates();
            if(pair != null)
                valores.put("coordinates", pair.first + "," + pair.second);
            valores.put("coffice",oficina.getCoffice());
            long result = db.insert("oficina", null, valores);
        }
        return true;
    }

    public ArrayList<Oficina> qryOficinas(){
        ArrayList<Oficina> oficinas = new ArrayList<>();
        String qry = "select * from oficina";
        Cursor c = executeQry(qry);
        if(c != null && c.moveToFirst()){
            do{
                Oficina oficina = new Oficina();
                oficina.setOffice(c.getString(0));
                oficina.setStreet_number(c.getString(1));
                oficina.setLocation(c.getString(2));
                oficina.setZip_code(c.getString(3));
                oficina.setState(c.getString(4));
                oficina.setCountry(c.getString(5));
                oficina.setAgent(c.getString(6));
                oficina.setPosition(c.getString(7));
                oficina.setEmail(c.getString(8));
                oficina.setPhone(c.getString(9));
                String cor = c.getString(10);
                if(cor != null && cor.length() > 10) {
                    String[] location = cor.split(",");
                    oficina.setCoordinates(Pair.create(location[0], location[1]));
                }
                oficina.setCoffice(c.getString(11));
                oficinas.add(oficina);
            }while(c.moveToNext());
        }
        return oficinas;
    }

    public int insertEncuesta(String foto,String usuario){
        ContentValues valores = new ContentValues();
        valores.put("foto",foto);
        valores.put("usuario",usuario);
        long result = db.insert("encuesta", null, valores);
        int cve_enc = 0;
        if(result != -1){
            Cursor c1= executeQry("select max(cve_enc)from encuesta");
            if (c1!=null && c1.moveToFirst()) {
                cve_enc = c1.getInt(0);
            }
        }
        return cve_enc;
    }

    public void insertPregunta(Pregunta pregunta,int cve){
        ContentValues valores = new ContentValues();
        valores.put("pregunta",pregunta.getPregunta());
        valores.put("respuesta",pregunta.respuesta);
        valores.put("cve_enc",cve);
        long result = db.insert("pregunta", null, valores);
    }


}
