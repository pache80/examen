package com.example.pache.examen.dataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by pache on 26/03/2016.
 */
public class DataBase  extends SQLiteOpenHelper {

    String tableOficiona = "CREATE TABLE oficina (office TEXT PRIMARY KEY,street_number text,location text,zip_code text, state text ," +
            "country text, agent text,position text,email text, phone text, coordinates text, coffice text)";
    String tableRegistro = "CREATE TABLE registro (usuario text PRIMARY KEY,contraseña text,nombre text)";
    String tablePregunta = "CREATE TABLE pregunta (cve_pre INTEGER PRIMARY KEY AUTOINCREMENT,pregunta text,respuesta text,cve_enc numeric)";
    String tableEncuesta = "CREATE TABLE encuesta (cve_enc INTEGER PRIMARY KEY AUTOINCREMENT,foto text,usuario text)";

    public DataBase(Context contexto, String nombre,
                                SQLiteDatabase.CursorFactory factory, int version) {
        super(contexto, nombre, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(tableOficiona);
        db.execSQL(tableRegistro);
        db.execSQL(tablePregunta);
        db.execSQL(tableEncuesta);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int versionAnterior, int versionNueva) {
    }


}
