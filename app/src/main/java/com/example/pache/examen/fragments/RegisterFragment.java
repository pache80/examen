package com.example.pache.examen.fragments;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.pache.examen.MainActivity;
import com.example.pache.examen.R;
import com.example.pache.examen.classes.Registro;
import com.example.pache.examen.common.Utilities;
import com.example.pache.examen.dataBase.QueryDatabase;


public class RegisterFragment extends Fragment {
    EditText txtPassword;
    EditText txtUser;
    EditText txtName;

    public RegisterFragment() {
        // Required empty public constructor
    }
    public static RegisterFragment newInstance() {
        RegisterFragment fragment = new RegisterFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        getActivity().setTitle("Registro");
        ((MainActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtPassword = (EditText)view.findViewById(R.id.pass);
        txtUser = (EditText)view.findViewById(R.id.usu);
        txtName = (EditText)view.findViewById(R.id.nom);
        Button btnRegister = (Button)view.findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (valida()) {
                    QueryDatabase consul = new QueryDatabase(getActivity());
                    Registro user = new Registro();
                    user.setContraseña(txtPassword.getText().toString());
                    user.setNombre(txtUser.getText().toString());
                    user.setUsuario(txtUser.getText().toString());
                    if(consul.insertUser(user)){
                        getFragmentManager().popBackStack();
                        Utilities.mensaje("Alerta","Registro exitoso",getActivity());
                    }
                }
            }
        });
        return view;
    }

    public boolean valida(){
        String message = "";
        if(txtName.getText().toString().equalsIgnoreCase(""))
            message += "- Ingrese su nombre \n";
        if(txtUser.getText().toString().equalsIgnoreCase(""))
            message += "- Ingrese el usuario \n";
        if(txtPassword.getText().toString().equalsIgnoreCase(""))
            message += "- Ingrese la contraseña \n";

        if(!message.equalsIgnoreCase(""))
            Utilities.mensaje("Los campos son requeridos", message, getActivity());
        return  message.equalsIgnoreCase("");
    }
}
