package com.example.pache.examen.classes;

/**
 * Created by pache on 28/03/2016.
 */
public class Pregunta {
    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String usuario) {
        Usuario = usuario;
    }

    public String pregunta;
    public String respuesta;
    public String Usuario;
}
