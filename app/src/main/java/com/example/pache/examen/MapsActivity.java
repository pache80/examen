package com.example.pache.examen;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.pache.examen.classes.Oficina;
import com.example.pache.examen.classes.Pregunta;
import com.example.pache.examen.common.Definitions;
import com.example.pache.examen.common.GPSTracker;
import com.example.pache.examen.common.Utilities;
import com.example.pache.examen.common.WorkaroundMapFragment;
import com.example.pache.examen.connection.Connection;
import com.example.pache.examen.dataBase.QueryDatabase;
import com.example.pache.examen.interfaces.WebServiceResponse;
import com.example.pache.examen.parse.ParseResponse;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener, WebServiceResponse {

    private GoogleMap mMap;
    ScrollView mScrollView;
    ProgressDialog dialog;
    Spinner spinner;
    TextView txtSelected;
    String img = "";
    EditText txtNom;
    boolean seletedIMage = false;
    String usuario = "";
    RadioGroup radioGroupOne;
    RadioGroup radioGroupTwo;
    RadioGroup radioGroupThree;



    private static final int RESULT_LOAD_IMAGE = 1;
    private static int TAKE_PICTURE = 1;
    private static int SELECT_PICTURE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            usuario  = extras.getString("usuario");
        }

        SupportMapFragment mapFragment = (WorkaroundMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        spinner = (Spinner) findViewById(R.id.spiner);
        mMap = ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        mScrollView = (ScrollView) findViewById(R.id.myScroll);
        txtSelected = (TextView) findViewById(R.id.imagenSelected);
        txtNom = (EditText) findViewById(R.id.txtNomPerson);
        radioGroupOne = (RadioGroup) findViewById(R.id.yesNoOption);
        radioGroupTwo = (RadioGroup) findViewById(R.id.information);
        radioGroupThree = (RadioGroup) findViewById(R.id.estadoLugar);


        Button btnSave = (Button) findViewById(R.id.finalizar);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });

                ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).setListener(new WorkaroundMapFragment.OnTouchListener() {
                    @Override
                    public void onTouch() {
                        mScrollView.requestDisallowInterceptTouchEvent(true);
                    }
                });
        Button btnImage = (Button) findViewById(R.id.addImg);
        btnImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                final CharSequence[] items = {"Cámara", "Galería"};
                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MapsActivity.this);
                dialogBuilder.setTitle("Selecciona una opción:");
                dialogBuilder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int position) {
                        switch (position) {
                            case 0:
                                ContentValues values = new ContentValues();
                                values.put(MediaStore.Images.Media.TITLE, "");
                                values.put(MediaStore.Images.Media.DESCRIPTION, "");
                                Uri mImageUri = getApplication().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
                                int code = SELECT_PICTURE;
                                startActivityForResult(intent, code);
                                dialog.dismiss();
                                break;
                            case 1:


                                if (Build.VERSION.SDK_INT < 19) {
                                    Intent intent = new Intent();
                                    intent.setType("image/jpeg");
                                    intent.setAction(Intent.ACTION_GET_CONTENT);
                                    startActivityForResult(Intent.createChooser(intent, ""), RESULT_LOAD_IMAGE);
                                } else {
                                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                                    intent.setType("image/jpeg");
                                    startActivityForResult(intent, RESULT_LOAD_IMAGE);
                                }

                                dialogBuilder.create().dismiss();
                                dialog.dismiss();
                                break;
                        }

                    }
                });
                AlertDialog dialog = dialogBuilder.create();
                dialog.show();
            }
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        dialog = ProgressDialog.show(this, "", "Cargando...", true);
        mMap = googleMap;
        GPSTracker gps = new GPSTracker(this);
        LatLng myLocation = new LatLng(gps.getLatitude(), gps.getLongitude());
        mMap.addMarker(new MarkerOptions().position(myLocation).title("My Location"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(myLocation));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f));
        if (isNetworkAvailable()) {
            Connection connection = new Connection(this);
            connection.requestCatalogue();
        } else {
            QueryDatabase queryDatabase = new QueryDatabase(this);
            ArrayList<Oficina> oficinas = queryDatabase.qryOficinas();
            addMarkers(oficinas);
            initSpiner(oficinas);
            dialog.dismiss();
        }
        copyDatabaseToSd();
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void responseCataloge(String response) {
        ParseResponse parseResponse = new ParseResponse();
        dialog.dismiss();

        ArrayList<Oficina> oficinas = parseResponse.parseCatalogue(response);
        addMarkers(oficinas);
        QueryDatabase qry = new QueryDatabase(this);
        qry.inserOficina(oficinas);
        initSpiner(oficinas);

    }

    @Override
    public void onResponseError(String error) {
        dialog.dismiss();
        Utilities.mensaje("Ocurrio un error", error, this);
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public void addMarkers(ArrayList<Oficina> oficinas) {
        for (Oficina oficina : oficinas) {
            Pair pair = oficina.getCoordinates();
            if (pair != null) {
                LatLng myLocation = new LatLng(Double.parseDouble(pair.first.toString()), Double.parseDouble(pair.second.toString()));
                mMap.addMarker(new MarkerOptions().position(myLocation).title(oficina.getOffice()));
            }
        }
    }

    public void copyDatabaseToSd() {

        try {
            File sd = Environment.getExternalStorageDirectory();
            if (sd.canWrite()) {
                String currentDBPath = "/data/data/" + getPackageName() + "/databases/" + Definitions.nameDataBase;
                File currentDB = new File(currentDBPath);
                File tempZip = new File(sd.getAbsolutePath() + "/" + "archivo.zip");
                ZipFile zipFile = new ZipFile(sd.getAbsolutePath() + "/" + "archivo.zip");
                ZipParameters parameters = new ZipParameters();
                parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
                parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_ULTRA);
                parameters.setEncryptFiles(true);
                parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_AES);
                parameters.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_256);
                parameters.setPassword("eduardo");
                zipFile.addFile(currentDB, parameters);
                Log.e("archivo copeado a: ", sd.getAbsolutePath() + "/" + "archivo.zip");
            }
        } catch (Exception e) {
            Log.e("ocurrio un error", e.getMessage());
        }
    }

    public void initSpiner(ArrayList<Oficina> oficinas) {
        List<String> list = new ArrayList<String>();
        for (Oficina oficina : oficinas) {
            if (oficina.getOffice() != null && oficina.getOffice().length() > 0)
                list.add(oficina.getOffice());
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(R.layout.spiner_layout);
        spinner.setAdapter(dataAdapter);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == TAKE_PICTURE) {

                if (requestCode == RESULT_LOAD_IMAGE && resultCode == this.RESULT_OK && null != data) {

                    if (data != null) {
                        String picturePath = getRealPathFromURI(data.getData());
                        Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, stream);
                        byte[] image = stream.toByteArray();
                        img = Base64.encodeToString(image, 0);
                        seletedIMage = true;
                        txtSelected.setVisibility(View.VISIBLE);
                    } else {
                        Uri selectedImage = data.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        Cursor cursor = this.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                        cursor.moveToFirst();
                        cursor.close();
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        byte[] image = stream.toByteArray();
                        img = Base64.encodeToString(image, 0);
                        seletedIMage = true;
                        txtSelected.setVisibility(View.VISIBLE);
                    }
                }
            }
            if (requestCode == SELECT_PICTURE) {

                if (data != null) {
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = this.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, filePathColumn, null, null, null);
                    if (cursor == null)
                        return;
                    cursor.moveToLast();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
                    cursor.close();
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 60, stream);
                    byte[] image = stream.toByteArray();
                    img = Base64.encodeToString(image, 0);
                    seletedIMage = true;
                    txtSelected.setVisibility(View.VISIBLE);
                } else {
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = this.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, filePathColumn, null, null, null);
                    if (cursor == null)
                        return;
                    cursor.moveToLast();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();
                    Bitmap aux = BitmapFactory.decodeFile(picturePath);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    try {
                        aux.compress(Bitmap.CompressFormat.JPEG, 60, stream);
                    }catch (Exception ex){
                        aux.compress(Bitmap.CompressFormat.JPEG, 45, stream);
                    }
                    byte[] image = stream.toByteArray();
                    img = Base64.encodeToString(image, 0);
                    seletedIMage = true;
                    txtSelected.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    public void save() {
        if (valida()) {
            QueryDatabase qry = new QueryDatabase(this);
            int cve = qry.insertEncuesta(img,usuario);
            Pregunta pregunta = new Pregunta();
            pregunta.setPregunta("Ingrese el nombre de la persona que lo atendió");
            pregunta.setRespuesta(txtNom.getText().toString());
            qry.insertPregunta(pregunta, cve);

            pregunta = new Pregunta();
            pregunta.setPregunta("¿El trato fue bueno?");
            pregunta.setRespuesta(selectedValue(radioGroupOne));
            qry.insertPregunta(pregunta, cve);

            pregunta = new Pregunta();
            pregunta.setPregunta("¿La información que le brindaron fue de ayuda?");
            pregunta.setRespuesta(selectedValue(radioGroupTwo));
            qry.insertPregunta(pregunta, cve);

            pregunta = new Pregunta();
            pregunta.setPregunta("¿El lugar se encontraba limpio?");
            pregunta.setRespuesta(selectedValue(radioGroupThree));
            qry.insertPregunta(pregunta, cve);

            pregunta = new Pregunta();
            pregunta.setPregunta("Selecciona la oficina de atencion");
            pregunta.setRespuesta(spinner.getSelectedItem().toString());
            qry.insertPregunta(pregunta, cve);


            Utilities.mensaje("Alerta","Gracias por responder nuestra encuesta",this);

        }
    }

    public boolean valida(){
        String message = "";
        if(!seletedIMage)
            message += "Selecciona una imagen";
        if(txtNom.getText().toString().equalsIgnoreCase(""))
            message += "Ingresa un nombre";
        if(!message.equalsIgnoreCase(""))
            Utilities.mensaje("Alerta",message,this);
        return message.equalsIgnoreCase("");
    }

    public String selectedValue(RadioGroup radioButtonGroup){
        int radioButtonID = radioButtonGroup.getCheckedRadioButtonId();
        View radioButton = radioButtonGroup.findViewById(radioButtonID);
        int idx = radioButtonGroup.indexOfChild(radioButton);

        RadioButton r = (RadioButton)  radioButtonGroup.getChildAt(idx);
        String selectedtext = r.getText().toString();
        return selectedtext;
    }

    @SuppressLint("NewApi")
    private String getRealPathFromURIAPI19(Uri contentUri) {
        String filePath = "";
        String wholeID = DocumentsContract.getDocumentId(contentUri);

        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];

        String[] column = {MediaStore.Images.Media.DATA};

        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";

        Cursor cursor = this.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                column, sel, new String[]{id}, null);

        int columnIndex = cursor.getColumnIndex(column[0]);

        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }

    @SuppressLint("NewApi")
    public String getRealPathFromURI_API11to18(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        String result = null;

        CursorLoader cursorLoader = new CursorLoader(
                this,
                contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        if (cursor != null) {
            int column_index =
                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
        }
        return result;
    }

    private String getRealPathFromURI(Uri contentUri) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return getRealPathFromURIAPI19(contentUri);
        } else {
            return getRealPathFromURI_API11to18(contentUri);
        }
    }

}
