package com.example.pache.examen.fragments;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.pache.examen.MainActivity;
import com.example.pache.examen.MapsActivity;
import com.example.pache.examen.R;
import com.example.pache.examen.classes.Registro;
import com.example.pache.examen.common.Utilities;
import com.example.pache.examen.dataBase.QueryDatabase;


public class LoginFragment extends Fragment {
    EditText txtPassword;
    EditText txtUser;
    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        ((MainActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getActivity().setTitle("Login");
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        txtPassword = (EditText)view.findViewById(R.id.pass);
        txtUser = (EditText)view.findViewById(R.id.usu);
        Button btnLogin = (Button)view.findViewById(R.id.buttonLogin);
        final FragmentTransaction transaction = getFragmentManager().beginTransaction();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(valida()){
                    QueryDatabase consul = new QueryDatabase(getActivity());
                    Registro user = new Registro();
                    user.setContraseña(txtPassword.getText().toString());
                    user.setUsuario(txtUser.getText().toString());
                    if(consul.exitsUser(user)){
                        Intent intent = new Intent(getActivity(), MapsActivity.class);
                        intent.putExtra("usuario",txtUser.getText().toString());
                        startActivity(intent);
                        getActivity().finish();
                    }
                }
            }
        });
        TextView txtRegistro = (TextView)view.findViewById(R.id.registro);
        txtRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transaction.replace(R.id.container, new RegisterFragment());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        return view;
    }

    public boolean valida(){
        String message = "";
        if(txtUser.getText().toString().equalsIgnoreCase(""))
            message += "- Ingrese el usuario \n";
        if(txtPassword.getText().toString().equalsIgnoreCase(""))
            message += "- Ingrese la contraseña \n";
        if(!message.equalsIgnoreCase(""))
            Utilities.mensaje("Los campos son requeridos",message,getActivity());
        return  message.equalsIgnoreCase("");
    }


}
