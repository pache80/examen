package com.example.pache.examen.parse;

import android.util.Log;
import android.util.Pair;

import com.example.pache.examen.classes.Oficina;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pache on 27/03/2016.
 */
public class ParseResponse {
    public ArrayList<Oficina> parseCatalogue(String response){
        try {
            JSONObject jsonResponse = new JSONObject(response);
            JSONArray catalogue = jsonResponse.getJSONArray("oficinasExterior");
            ArrayList<Oficina> oficinas = new ArrayList<>();
            for(int index = 0; index < catalogue.length(); index ++) {
                JSONObject json_data = catalogue.getJSONObject(index);
                Oficina oficina = new Oficina();
                oficina.setOffice(json_data.getString("Office"));
                oficina.setStreet_number(json_data.getString("Street_number"));
                oficina.setLocation(json_data.getString("Location"));
                oficina.setZip_code(json_data.getString("zip_code"));
                oficina.setState(json_data.getString("State"));
                oficina.setCountry(json_data.getString("Country"));
                oficina.setAgent(json_data.getString("Agent"));
                oficina.setPosition(json_data.getString("Position"));
                oficina.setEmail(json_data.getString("Email"));
                oficina.setPhone(json_data.getString("Phone"));
                if(json_data.getString("Coordinates").length() > 10) {
                    String[] location = json_data.getString("Coordinates").split(",");
                    oficina.setCoordinates(Pair.create(location[0], location[1]));
                }
                oficina.setCoffice(json_data.getString("COffice"));
                oficinas.add(oficina);
            }
            return oficinas;
        } catch (JSONException e) {
            Log.e("ocurrio un error",e.getMessage());
        }
        return null;
    }
}
