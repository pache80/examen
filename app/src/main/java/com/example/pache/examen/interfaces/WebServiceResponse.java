package com.example.pache.examen.interfaces;

/**
 * Created by pache on 27/03/2016.
 */
public interface WebServiceResponse {
    public void responseCataloge(String response);
    public void onResponseError(String error);
}
